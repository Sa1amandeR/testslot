using UnityEngine;

public class LowerBalancePopup : MonoBehaviour, IPopup
{
    public void OnCloseClick()
    {
        Destroy(this);
    }

    public void ToLobby()
    {
        RequestManager.instance.SendRequest("LeaveRoom");
    }
}
