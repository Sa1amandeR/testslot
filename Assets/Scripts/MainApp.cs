using UnityEngine;

public class MainApp : MonoBehaviour, ISubscribable
{
    private void Awake()
    {
        NotificationManager.instance.Subscribe(NotificationsEnum.GAME_START_AVAILABEL, this);

        ResourceManager.instance.Init();
        UserDataManager.instance.Init();

        //DontDestroyOnLoad(this);
        //Application.targetFrameRate = 300;
    }

    public void NotificationReceive(NotificationsEnum type, object data)
    {
        switch (type)
        {
            case NotificationsEnum.GAME_START_AVAILABEL:
                RequestManager.instance.SendRequest("Login");
                break;
        }
    }
}
