using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemsGenerator
{
    public static List<ReelItemDTO> BuilItemsStrip(List<ItemConfigDTO> itemsDTOs, int stripLength, GameObject templateGO)
    {
        List<ReelItemDTO> newItemsStrip = new List<ReelItemDTO>();
        for (int i = 0; i < stripLength; i++)
        {
            ReelItemDTO newItemOnject = CreateSingleRandomItem(itemsDTOs, templateGO);
            newItemOnject.itemGO.transform.SetPositionAndRotation(new Vector3(0, i * newItemOnject.height, 0), Quaternion.identity);

            newItemsStrip.Add(newItemOnject);
        }

        return newItemsStrip;
    }

    public static ReelItemDTO CreateSingleRandomItem(List<ItemConfigDTO> itemsDTOs, GameObject templateGO)
    {
        int randomIndex = Random.Range(1, itemsDTOs.Count + 1) - 1;
        ItemConfigDTO dto = itemsDTOs[randomIndex];
        GameObject newItemOnject = Object.Instantiate(templateGO) as GameObject;
        newItemOnject.GetComponent<Image>().sprite = dto.Image;

        ReelItemDTO newReelItemDto = new ReelItemDTO
        {
            itemId = dto.ServerItemId,
            itemGO = newItemOnject,
            height = dto.ItemHeight,
            width = dto.ItemWidth,
            isPointForStartStopping = false
        };

        return newReelItemDto;
    }

    public static ReelItemDTO CreateItemByServerId(List<ItemConfigDTO> itemsDTOs, GameObject templateGO, string serverId)
    {
        ItemConfigDTO itemDto = itemsDTOs.Find((dto) => dto.ServerItemId == serverId);
        GameObject newItemOnject = Object.Instantiate(templateGO) as GameObject;
        newItemOnject.GetComponent<Image>().sprite = itemDto.Image;

        ReelItemDTO newReelItemDto = new ReelItemDTO
        {
            itemId = itemDto.ServerItemId,
            itemGO = newItemOnject,
            height = itemDto.ItemHeight,
            width = itemDto.ItemWidth,
            isPointForStartStopping = false
        };

        return newReelItemDto;
    }
}
