﻿using UnityEngine;
using System.Collections;

public static class ExternalMonoFeaturesUse
{
    private class MonoFeaturesRunner : MonoBehaviour {};

    private static MonoFeaturesRunner _runner;
    private static MonoFeaturesRunner runner
    {
        get {
            if (_runner == null)
            {
                _runner = new GameObject("MonoFeaturesRunner").AddComponent<MonoFeaturesRunner>();
            }

            return _runner;
        }
    }

    #region Coroutine
    public static Coroutine StartCoroutine(IEnumerator coroutine)
    {
        return runner.StartCoroutine(coroutine);
    }

    public static void StopCoroutine(Coroutine coroutine)
    {
        runner.StopCoroutine(coroutine);
    }

    public static void StopAllCoroutine()
    {
        runner.StopAllCoroutines();
    }
    #endregion
}
