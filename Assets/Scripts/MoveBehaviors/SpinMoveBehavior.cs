using System.Collections.Generic;
using UnityEngine;

public class SpinMoveBehavior : IMoveBehavior
{
    private float deltaY;

    public SpinMoveBehavior()
    {
        #if UNITY_STANDALONE_WIN
            deltaY = 30;
        #endif

        #if UNITY_EDITOR_WIN
            deltaY = 5;
        #endif
    }
    public void Evaluate(List<ReelItemDTO> movemenItems)
    {
        movemenItems.ForEach((item) =>
        {
            float currentYpos = item.itemGO.transform.localPosition.y;
            item.itemGO.transform.localPosition = new Vector3(0, currentYpos - deltaY, 0);
        });
    }

    public void ResetToDefault()
    {
        
    }
}
