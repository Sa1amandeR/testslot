using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartSpinMoveBehavior : IMoveBehavior
{
    private float startDeltaY;
    private float endDeltaY;
    private int frameCount;
    private int duration;

    public StartSpinMoveBehavior()
    {
        #if UNITY_STANDALONE_WIN
            startDeltaY = -5;
            endDeltaY = 35;
            frameCount = 0;
            duration = 25;
        #endif

        #if UNITY_EDITOR_WIN
            startDeltaY = -1;
            endDeltaY = 6;
            frameCount = 0;
            duration = 120;
        #endif
    }

    public void Evaluate(List<ReelItemDTO> movemenItems)
    {
        if(frameCount < duration)
        {
            frameCount = ++frameCount;

            float t = frameCount / duration;
            float currentDeltaY = -endDeltaY * t * t * t - startDeltaY;

            movemenItems.ForEach((item) =>
            {
                float currentYpos = item.itemGO.transform.localPosition.y;
                item.itemGO.transform.localPosition = new Vector3(0, currentYpos + currentDeltaY, 0);
            });
        } else
        {
            frameCount = duration;
            NotificationManager.instance.Dispatch(NotificationsEnum.REEL_STARTED, movemenItems[0].reelId);
        }
    }

    public void ResetToDefault()
    {
        frameCount = 0;
    }
}
