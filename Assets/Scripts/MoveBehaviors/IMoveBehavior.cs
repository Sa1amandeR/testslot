using System.Collections.Generic;

public interface IMoveBehavior
{
    void Evaluate(List<ReelItemDTO> movemenItems);
    void ResetToDefault();
}
