using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopSpinBehavior : IMoveBehavior
{
    private int startBounceFrames;
    private int endBounceFrames;
    private int speedBeforePoinReached;
    private int startBounceSpeed;
    private float multiplierStartBounce;
    private float multiplierStopBounce;

    private float startBounceFrameCount = 0;
    private float endBounceFrameCount = 0;
    private float currentPointPositionY;
    private bool _isPointerReachedBottom = false;
    private bool _isStartBounceFinish = false;
    private bool _isEndBounceFinish = false;
    private ReelItemDTO _pointerItem;
    private float _updatedSpeed = 0;

    public StopSpinBehavior()
    {
        #if UNITY_STANDALONE_WIN
            startBounceFrames = 20;
            endBounceFrames = 50;
            speedBeforePoinReached = 30;
            startBounceSpeed = 20;
            multiplierStartBounce = 0.18f;
            multiplierStopBounce = 0.02f;
    #endif

    #if UNITY_EDITOR_WIN
        startBounceFrames = 120;
            endBounceFrames = 120;
            speedBeforePoinReached = 5;
            startBounceSpeed = 5;
            multiplierStartBounce = 0.09f;
            multiplierStopBounce = 0.003f;
    #endif
    }

    public void Evaluate(List<ReelItemDTO> movemenItems)
    {
        if (!_isPointerReachedBottom)
        {
            movemenItems.ForEach((item) =>
            {
                if (item.isPointForStartStopping)
                {
                    currentPointPositionY = item.itemGO.transform.localPosition.y;
                    if (currentPointPositionY <= 0)
                    {
                        _pointerItem = item;
                        _isPointerReachedBottom = true;
                    }
                }
                float currentYpos = item.itemGO.transform.localPosition.y;
                item.itemGO.transform.localPosition = new Vector3(0, currentYpos - speedBeforePoinReached, 0);
            });
        } 

        if(_isPointerReachedBottom && !_isStartBounceFinish)
        {
            StartBounce(movemenItems);
        }

        if (_isPointerReachedBottom && _isStartBounceFinish && !_isEndBounceFinish)
        {
            FinishBounce(movemenItems);
        }
    }

    public void StartBounce(List<ReelItemDTO> movemenItems)
    {
        _updatedSpeed += multiplierStartBounce * speedBeforePoinReached;
        if(Mathf.Abs(_pointerItem.itemGO.transform.localPosition.y) < _pointerItem.height)
        {
            SetItemsPosition(movemenItems, -_updatedSpeed);
        } else
        {
            _updatedSpeed = 0;
            SetItemsPosition(movemenItems, _updatedSpeed);
            _isStartBounceFinish = true;
        }

        /*if (startBounceFrameCount < startBounceFrames) {
            startBounceFrameCount = ++startBounceFrameCount;

            float delta = startBounceFrameCount / startBounceFrames;
            _updatedSpeed = startBounceSpeed + startBounceSpeed * delta * (delta - 2);
            
            SetItemsPosition(movemenItems, -_updatedSpeed);
        } else
        {
            _isStartBounceFinish = true;
        }*/
    }

    public void FinishBounce(List<ReelItemDTO> movemenItems)
    {
        _updatedSpeed += multiplierStopBounce * speedBeforePoinReached;
        if (_pointerItem.itemGO.transform.localPosition.y < 0)
        {
            SetItemsPosition(movemenItems, _updatedSpeed);
        }
        else
        {
            SetItemsPosition(movemenItems, -_pointerItem.itemGO.transform.localPosition.y);
            _isEndBounceFinish = true;
            NotificationManager.instance.Dispatch(NotificationsEnum.REEL_STOPED, movemenItems[0].reelId);
        }
        /*if (endBounceFrameCount < endBounceFrames)
        {
            endBounceFrameCount = ++endBounceFrameCount;

            float delta = endBounceFrameCount / endBounceFrames;
            _updatedSpeed = startBounceSpeed + startBounceSpeed * delta * (delta - 2);//quadratic ease-out

            SetItemsPosition(movemenItems, -_updatedSpeed);
        } else
        {
            if (_pointerItem.transform.localPosition.y < 0)
            {
                _updatedSpeed = -_pointerItem.transform.localPosition.y * 0.3f;
            } else
            {
                _updatedSpeed = -_pointerItem.transform.localPosition.y;
            }
          
            SetItemsPosition(movemenItems, _updatedSpeed);

            if(Mathf.RoundToInt(_updatedSpeed) == Mathf.RoundToInt(_pointerItem.transform.localPosition.y))
            {
                _isEndBounceFinish = true;
                NotificationManager.instance.Dispatch(NotificationsEnum.REEL_STOPED, movemenItems[0].reelId);
            }
        }*/
    }

    public void ResetToDefault()
    {
        
    }

    private void SetItemsPosition(List<ReelItemDTO> movemenItems, float deltaValue)
    {
        movemenItems.ForEach((item) =>
        {
            float currentYpos = item.itemGO.transform.localPosition.y;
            item.itemGO.transform.localPosition = new Vector3(0, currentYpos + deltaValue, 0);
        });
    }
}
