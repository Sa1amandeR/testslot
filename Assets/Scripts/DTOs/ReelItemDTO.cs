using UnityEngine;

public class ReelItemDTO
{
    public string itemId;
    public int reelId;
    public GameObject itemGO;
    public int height;
    public int width;
    public bool isPointForStartStopping;

    public void DestroyItem()
    {
        itemId = null;
        GameObject.Destroy(itemGO);
    }
}
