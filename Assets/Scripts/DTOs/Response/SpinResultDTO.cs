using System;
using System.Collections.Generic;

[Serializable]
public class SpinResultDTO
{
    public List<ReelResultDTO> reelsData;
    public List<ComboResultDTO> combo;
}
