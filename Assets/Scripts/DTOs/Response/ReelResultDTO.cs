using System;

[Serializable]
public class ReelResultDTO
{
    public int id;
    public string[] symbols;
}
