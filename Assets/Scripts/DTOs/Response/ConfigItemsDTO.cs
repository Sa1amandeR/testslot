using System;

[Serializable]
public class ConfigItemsDTO
{
    public string[] symbols;
    public float[] chances;
    public ConfigComboDTO[] combos;
}
