[System.Serializable]
public class UserDTO
{
    public int balance;
    public int id;
    public int spins;
    public string login;
    public string password;
    public string updatedAt;
    public string createdAt;
}
