using System;

[Serializable]
public class SpinDTO
{
    public int id;
    public RewardDTO reward;
    public int userId;
    public int slotMachineId;
    public SpinResultDTO result;
}
