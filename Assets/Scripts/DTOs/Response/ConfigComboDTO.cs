using System;

[Serializable]
public class ConfigComboDTO
{
    public string[] combo;
    public RewardDTO reward;
}
