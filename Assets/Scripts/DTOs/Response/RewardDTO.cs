using System;

[Serializable]
public class RewardDTO
{
    public int coins;
    public int spins;
}
