public class ResponseDTO
{
    public bool success;
    public string message;
    public ConfigDTO config;
    public SpinDTO data;
}
