using System;
using System.Collections.Generic;

[Serializable]
public class ComboResultDTO
{
    public int lineId;
    public List<ItemsCoordinateDTO> itemsCoordinate;
}
