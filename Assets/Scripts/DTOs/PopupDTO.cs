using System;
using UnityEngine;

[Serializable]
public class PopupDTO
{
    public string name;
    public GameObject popup;
}
