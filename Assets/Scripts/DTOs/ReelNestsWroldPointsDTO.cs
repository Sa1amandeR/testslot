using System.Collections.Generic;
using UnityEngine;

public class ReelNestsWroldPointsDTO {
    public int reelId;
    public Vector3 leftBottomCorner;
    public Vector3 rightBottomCorner;
    public List<Vector3> nestPoints;
}
