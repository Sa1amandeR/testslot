using System;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public class ItemConfigDTO
{
    [SerializeField]
    private int itemId;
    [SerializeField]
    private Sprite image;
    [SerializeField]
    private string serverItemId;
    [SerializeField]
    private int itemWidth;
    [SerializeField]
    private int itemHeight;

    public int ItemId
    {
        get
        {
            return itemId;
        }
    }

    public Sprite Image
    {
        get
        {
            return image;
        }
    }

    public string ServerItemId
    {
        get
        {
            return serverItemId;
        }

        set
        {
            serverItemId = value;
        }
    }

    public int ItemWidth
    {
        get
        {
            return itemWidth;
        }
    }

    public int ItemHeight
    {
        get
        {
            return itemHeight;
        }
    }
}
