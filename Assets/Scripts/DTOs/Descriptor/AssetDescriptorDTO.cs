using System;

[Serializable]
public class AssetDescriptorDTO
{
    public string type;
    public string filePath;
}
