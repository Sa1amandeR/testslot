using System.Collections.Generic;
using System;

[Serializable]
public class LocalResponseDescriptorDTO
{
    public string type;
    public List<string> filesNames;
}
