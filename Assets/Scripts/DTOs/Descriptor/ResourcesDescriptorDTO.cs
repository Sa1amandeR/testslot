using System.Collections.Generic;

public class ResourcesDescriptorDTO
{
    public List<AssetDescriptorDTO> aseetsDescriptors;
    public List<LocalResponseDescriptorDTO> localResponsesDescription;
}
