using static NotificationManager;

public interface ISubscribable
{
   void NotificationReceive(NotificationsEnum type, object data = null);
}
