using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NotificationManager
{
    private Dictionary<NotificationsEnum, List<ISubscribable>> notificationsCollection;

    #region NotificationManager singletone instance 
    private static NotificationManager _instance;

    private NotificationManager()
    {
        notificationsCollection = new Dictionary<NotificationsEnum, List<ISubscribable>>();
    }

    public static NotificationManager instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new NotificationManager();
            }

            return _instance;
        }
    }
    #endregion

    public void Subscribe(NotificationsEnum notifType, ISubscribable target)
    {
        List<ISubscribable> subscribersList;
        if (!notificationsCollection.ContainsKey(notifType))
        {
            subscribersList = new List<ISubscribable>();
        } else {
            subscribersList = notificationsCollection[notifType];
        }

        if (!subscribersList.Contains(target))
        {
            subscribersList.Add(target);
        } else {
            Debug.LogFormat("{0} already subscribed!", target);
        }

        notificationsCollection[notifType] = subscribersList;
    }

    public void Unsubscribe(NotificationsEnum notifType, ISubscribable target)
    {
        if (notificationsCollection.ContainsKey(notifType))
        {
            List<ISubscribable>  subscribersList = notificationsCollection[notifType];
            if (subscribersList.Contains(target))
            {
                subscribersList.Remove(target);
            } else {
                Debug.LogFormat("{0} already unsubscribed!", target);
            }   
        } else {
            Debug.LogFormat("{0} not registered. Check 'notifType' parametr.", notifType);
        }
    }

    public void Dispatch(NotificationsEnum notifType, object data = null)
    {
        if (notificationsCollection.ContainsKey(notifType))
        {
            List<ISubscribable> subscribersList = notificationsCollection[notifType];
            subscribersList.ForEach((target) => target.NotificationReceive(notifType, data));
        }
        else
        {
            Debug.LogFormat("Dispatch {0} not available. {0} not registered. Check 'notifType' parametr.", notifType);
        }
    }
}

public enum NotificationsEnum
{
    //Load resources
    DESCRIPTOR_LOADED,
    GAME_START_AVAILABEL,
    SLOTMACHINE_LOADED,

    //Gameplay response recieve
    RESPONSE_RECIEVED,
    UPDATE_CURRENT_SPIN_DATA,

    //Notifications from reel
    UPDATE_REEL_STRIP,

    //Change reel behavior
    REEL_START_AVAILABLE,
    REEL_STOP_AVAILABLE,
    REEL_STARTED,
    REEL_STOPED,

    //Notification Panel
    UNLOCK_PANEL,
    LOCK_PANEL,
    SPIN_START,
    USER_LOCAL_DATA_UPDATED,
    SHOW_WINSCORE,

    //WinLines
    SHOW_WINLINES,
    REEL_NEST_POINTS,
    SHOW_WINLINE_ITEMS,
    REMOVE_WINLINE_ITEMS,

    //User data
    USER_LOGIN_COMPLETE,
    UPDATE_USER_LOCAL_DATA,

    //Increase balance
    BALANCE_UPDATED,

    JOIN_ROOM,
    LEAVE_ROOM,

    //Popups
    POPUP_SHOW
}
