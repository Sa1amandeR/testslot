using UnityEngine;

public class UserDataManager: ISubscribable
{
    private UserDTO _userData;
    private int _bet = 200;

    #region UserDataManager instance create
    private static UserDataManager _instance;
    public UserDataManager()
    {
        if (_instance == null)
        {

        }
        else
        {
            Debug.Log("RequestManager allready exist its singltone");
        }
    }

    public static UserDataManager instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new UserDataManager();
            }

            return _instance;
        }
    }

    public void Init() {
        NotificationManager.instance.Subscribe(NotificationsEnum.UPDATE_CURRENT_SPIN_DATA, this);
        NotificationManager.instance.Subscribe(NotificationsEnum.USER_LOGIN_COMPLETE, this);
        NotificationManager.instance.Subscribe(NotificationsEnum.BALANCE_UPDATED, this);
        NotificationManager.instance.Subscribe(NotificationsEnum.UPDATE_USER_LOCAL_DATA, this);
        NotificationManager.instance.Subscribe(NotificationsEnum.SPIN_START, this);
    }
    #endregion

    public UserDTO UserData() {
        return _userData;
    }

    public int GetBetCost()
    {
        return _bet;
    }

    public void NotificationReceive(NotificationsEnum type, object data)
    {
        switch (type)
        {
            case NotificationsEnum.BALANCE_UPDATED:
                _userData.balance = (int)data;
                break;
            case NotificationsEnum.UPDATE_USER_LOCAL_DATA:
                UpdateUserData(data as RewardDTO);
                break;
            case NotificationsEnum.USER_LOGIN_COMPLETE:
                _userData = data as UserDTO;
                break;
            case NotificationsEnum.SPIN_START:
                SpinStartUpdateBalance();
                break;
        }

        NotificationManager.instance.Dispatch(NotificationsEnum.USER_LOCAL_DATA_UPDATED);
    }

    private void SpinStartUpdateBalance()
    {
        if (_userData.spins > 0)
        {
            _userData.spins--;
        } else
        {
            _userData.balance -= _bet;
        }
    }

    private void UpdateUserData(RewardDTO reward)
    {
        if (reward != null)
        {
            _userData.balance += reward.coins;
            _userData.spins += reward.spins;
        }
    }
}
