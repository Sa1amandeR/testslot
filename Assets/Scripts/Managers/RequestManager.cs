﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

public class RequestManager
{
    private string networkType;
    #region RequestManager singletone instance 
    private static RequestManager _instance;

    public RequestManager()
    {
        if (_instance == null)
        {
            networkType = "local";
        }
        else
        {
            Debug.Log("RequestManager allready exist its singltone");
        }
    }

    public static RequestManager instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new RequestManager();
            }

            return _instance;
        }
    }

    public void Init() { }
    #endregion

    public void SendRequest(string requestType, string methodType = "POST", object jsonSendData = null)
    {
        string fullUrl = "";
        if (networkType == "local")
        {
            fullUrl = UrlBuilder.GetFullLocalUrlByActionType(requestType, Application.streamingAssetsPath);
            ExternalMonoFeaturesUse.StartCoroutine(GetRequest(fullUrl, requestType));
        } else {
            fullUrl = UrlBuilder.GetFullUrlByActionType(requestType, Application.streamingAssetsPath, 1);
            if (methodType == "POST")
            {
                string jsonString = JsonUtility.ToJson(jsonSendData);
                ExternalMonoFeaturesUse.StartCoroutine(PostRequest(fullUrl, jsonString, requestType));
            }
            else {
                ExternalMonoFeaturesUse.StartCoroutine(GetRequest(fullUrl, requestType));
            }
        }
    }

    //Write builder method for request data when server be available
    //Good point is put all request process in builder method/separated class, and return object e.g "RequestData". Given object send to "SendRequest" method.
    /*public RequestData BuildRequest()
    {

    }*/

    IEnumerator GetRequest(string url, string requestType)
    {
        string responseJsonString = "";
        UnityWebRequest uWebRequest = UnityWebRequest.Get(url);
        uWebRequest.SendWebRequest();

        while (!uWebRequest.isDone)
        {
            yield return null;
        }
        
        if (uWebRequest.result == UnityWebRequest.Result.ConnectionError)
        {
            Debug.Log("Send Error: " + uWebRequest.error);
        } else if (uWebRequest != null)
        {
            responseJsonString = uWebRequest.downloadHandler.text;
        }

        ResponseNotificationDTO notifDto = new ResponseNotificationDTO
        {
            requestType = requestType,
            responseDataString = responseJsonString
        };

        NotificationManager.instance.Dispatch(NotificationsEnum.RESPONSE_RECIEVED, notifDto);
    }

    IEnumerator PostRequest(string url, string jsonData, string requestType)
    {
        string responseJsonString = "";

        UnityWebRequest uWebRequest = new UnityWebRequest(url, "POST");
        uWebRequest.SetRequestHeader("Conent-Type", "application/json");
        uWebRequest.uploadHandler = new UploadHandlerRaw(Encoding.UTF8.GetBytes(jsonData));
        uWebRequest.downloadHandler = new DownloadHandlerBuffer();

        uWebRequest.SendWebRequest();

        while (!uWebRequest.isDone)
        {
            yield return null;
        }

        if (uWebRequest.result == UnityWebRequest.Result.ConnectionError)
        {
            Debug.Log("Send Error: " + uWebRequest.error);
        }
        else if (uWebRequest != null)
        {
            responseJsonString = uWebRequest.downloadHandler.text;
        }

        ResponseNotificationDTO notifDto = new ResponseNotificationDTO
        {
            requestType = requestType,
            responseDataString = responseJsonString
        };

        NotificationManager.instance.Dispatch(NotificationsEnum.RESPONSE_RECIEVED, notifDto);
    }
}

internal class UrlBuilder
{
    public static string GetFullUrlByActionType(string actionType, string baseUrl, int currentGameId)
    {
        string fullUrl = baseUrl;
        switch (actionType)
        {
            case "Login":
                fullUrl += "/login";
                break;
            case "Register":
                fullUrl += "/register";
                break;
            case "Config":
                fullUrl += "/config";
                break;
            case "Balance":
                fullUrl += "/user/balance";
                break;
            case "JoinRoom":
                fullUrl += String.Format("/room/{0}/join", currentGameId);
                break;
            case "LeaveRoom":
                fullUrl += "/room/leave";
                break;
            case "Spin":
                fullUrl += "/game/roll";
                break;
        }

        return fullUrl;
    }

    public static string GetFullLocalUrlByActionType(string actionType, string baseUrl)
    {
        string fullUrl = baseUrl;
        LocalResponseDescriptorDTO descriptor = ResourceManager.instance.GetLocalResponseDescriptor(actionType);
        string fileName = "";
        switch (actionType)
        {
            case "Login":
            case "Register":
            case "Config":
            case "Balance":
            case "JoinRoom":
            case "LeaveRoom":
                fileName = descriptor.filesNames[0];
                fullUrl = Path.Combine(baseUrl, "LocalResponses", fileName + ".json");
                break;
            case "Spin":
                fileName = GetNextSpinFileName(descriptor.filesNames);
                fullUrl = Path.Combine(baseUrl, "LocalResponses", "spins", fileName + ".json");
                break;
        }

        return fullUrl;
    }

    private static int spinCount = 0;
    private static string GetNextSpinFileName(List<string> filesNames)
    {
        string fileName;

        if (spinCount >= filesNames.Count)
        {
            spinCount = 0;
        }

        fileName = filesNames[spinCount];
        spinCount++;

        return fileName;
    }
}

enum RequestTypeEnum
{
    Config,
    Login,
    UserRegister,
    Spins,
    Balance
}


