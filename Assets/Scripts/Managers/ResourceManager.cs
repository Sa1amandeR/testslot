using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceManager: ISubscribable
{
    private ResourcesDescriptorDTO _resourceDescriptor;
    private ConfigDTO _config;
    private SpinDTO _currentSpin;
    private SpinDTO _previousSpin;

    #region ResourceManager singletone instance 
    private static ResourceManager _instance;

    private ResourceManager()
    {
        NotificationManager.instance.Subscribe(NotificationsEnum.DESCRIPTOR_LOADED, this);
        NotificationManager.instance.Subscribe(NotificationsEnum.RESPONSE_RECIEVED, this);
    }

    public static ResourceManager instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new ResourceManager();
            }

            return _instance;
        }
    }

    public void Init() {
        if(_resourceDescriptor == null)
        {
            Loader.GetInstance().LoadSettingDescription("game_descriptor");
        }
    }
    #endregion

    public void NotificationReceive(NotificationsEnum type, object data)
    {
        ResponseNotificationDTO jsonString = data as ResponseNotificationDTO;
        switch (type)
        {
            case NotificationsEnum.DESCRIPTOR_LOADED:
                string loadedDto = data as string;
                _resourceDescriptor = JsonUtility.FromJson<ResourcesDescriptorDTO>(loadedDto);
                NotificationManager.instance.Dispatch(NotificationsEnum.GAME_START_AVAILABEL);
                break;
            case NotificationsEnum.RESPONSE_RECIEVED:
                ResponseNotificationDTO responseRecievedDto = data as ResponseNotificationDTO;
                ResponseParse(responseRecievedDto);
                break;
        }
    }

    public LocalResponseDescriptorDTO GetLocalResponseDescriptor(string type)
    {
        return _resourceDescriptor.localResponsesDescription.Find((dto) => dto.type == type);
    }

    public AssetDescriptorDTO GetAssetDescriptor(string type)
    {
        return _resourceDescriptor.aseetsDescriptors.Find((dto) => dto.type == type);
    }

    private void ResponseParse(ResponseNotificationDTO responseRecieved)
    {
        string requestType = responseRecieved.requestType;
        ResponseDTO gameDataResponseDto = JsonUtility.FromJson<ResponseDTO>(responseRecieved.responseDataString);
        switch (requestType)
        {
            case "Config":
                _config = gameDataResponseDto.config;
                break;
            case "Login":
                LoginAndBalanceDTO loginDto = JsonUtility.FromJson<LoginAndBalanceDTO>(responseRecieved.responseDataString);
                NotificationManager.instance.Dispatch(NotificationsEnum.USER_LOGIN_COMPLETE, loginDto.data);
                break;
            case "Balance":
                LoginAndBalanceDTO balanceDto = JsonUtility.FromJson<LoginAndBalanceDTO>(responseRecieved.responseDataString);
                NotificationManager.instance.Dispatch(NotificationsEnum.BALANCE_UPDATED, balanceDto.data.balance);
                break;
            case "JoinRoom":
                ResponseDTO joinRoomResponse = JsonUtility.FromJson<ResponseDTO>(responseRecieved.responseDataString);
                if (joinRoomResponse.success)
                {
                    NotificationManager.instance.Dispatch(NotificationsEnum.JOIN_ROOM);
                }
                break;
            case "LeaveRoom":
                ResponseDTO leaveRoomResponse = JsonUtility.FromJson<ResponseDTO>(responseRecieved.responseDataString);
                if (leaveRoomResponse.success)
                {
                    NotificationManager.instance.Dispatch(NotificationsEnum.LEAVE_ROOM);
                }
                break;
            case "Spin":
                _currentSpin = gameDataResponseDto.data;
                //DelayedInvoke use for simulate response delay
                ExternalMonoFeaturesUse.StartCoroutine(DelayedInvoke());
                break;
        }
    }

    IEnumerator DelayedInvoke()
    {
        yield return new WaitForSeconds(2f);
        NotificationManager.instance.Dispatch(NotificationsEnum.UPDATE_CURRENT_SPIN_DATA, _currentSpin);
    }
}
