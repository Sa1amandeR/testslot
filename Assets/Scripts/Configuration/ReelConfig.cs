using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ReelConfig
{
    [SerializeField]
    private int reelId;
    [SerializeField]
    private int itemsInReel;
    [SerializeField]
    private ReelController reelController;

    #region getters
    public int ReelId {
        get
        {
            return reelId;
        }
    }

    public int ItemsInReel
    {
        get
        {
            return itemsInReel;
        }
    }

    public ReelController ReelController
    {
        get
        {
            return reelController;
        }
    }
    #endregion
}
