using System;
using System.Collections.Generic;
using UnityEngine;

public class GameItemsConfiguration : MonoBehaviour
{
    [SerializeField]
    private List<ReelConfig> reelsConfig;
    [SerializeField]
    private List<ItemConfigDTO> itemsConfig;

    [SerializeField]
    private GameObject itemTemplatePrefab;

    #region getters
    public List<ReelConfig> ReelsConfig
    {
        get
        {
            return reelsConfig;
        }
    }

    public List<ItemConfigDTO> ItemsConfig
    {
        get
        {
            return itemsConfig;
        }
    }
    public GameObject ItemTemplatePrefab
    {
        get
        {
            return itemTemplatePrefab;
        }
    }
    #endregion

    public ItemConfigDTO GetItemById(int itemLocalId)
    {
        return itemsConfig.Find((dto) => dto.ItemId == itemLocalId);
    }

    public ItemConfigDTO GetItemByServerId(string itemServerId)
    {
        return itemsConfig.Find((dto) => dto.ServerItemId == itemServerId);
    }
}
