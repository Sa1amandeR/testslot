using System.Collections.Generic;
using UnityEngine;

public class ReelsConfiguration : MonoBehaviour
{
    [SerializeField] private List<ReelConfig> reelsConfig;

    public List<ReelConfig> ReelsConfig
    {
        get
        {
            return reelsConfig;
        }
    }

    public ReelConfig GetReelConfigById(int id)
    {
        return reelsConfig.Find((config) => config.ReelId == id);
    }
}
