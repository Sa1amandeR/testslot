﻿using UnityEngine;
using System.IO;
using UnityEngine.Networking;
using System.Collections;

public class Loader
{
    private static Loader instance;

    private readonly string streamingAssetsFolderPath;

    #region Loader instance create
    private Loader(){
        streamingAssetsFolderPath = Application.streamingAssetsPath;
    }
    
    public static Loader GetInstance()
    {
        if (instance == null)
            instance = new Loader();
        return instance;
    }
    #endregion

    public void LoadSettingDescription(string fileName)
    {
        string configPath = Path.Combine(Application.streamingAssetsPath, fileName + ".json");
        ExternalMonoFeaturesUse.StartCoroutine(LoadJSONFile(configPath, NotificationsEnum.DESCRIPTOR_LOADED));
    }

    IEnumerator LoadJSONFile(string path, NotificationsEnum notifTypeOnLoaded)
    {
        string json = "";

        UnityWebRequest uWebRequest = UnityWebRequest.Get(path);
        uWebRequest.SendWebRequest();
        while (!uWebRequest.isDone)
        {
            yield return null;
        }

        if (uWebRequest != null)
        {
            json = uWebRequest.downloadHandler.text;
        }
        //else Debug.Log("ANDROID WEBREQUEST NULL FOR - " + path);

        //LoadedJsonDTO dto = new LoadedJsonDTO();
        //dto.dtoType = type;
        //dto.jsonString = json;

        NotificationManager.instance.Dispatch(notifTypeOnLoaded, json);
    }
}
