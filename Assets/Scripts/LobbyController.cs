using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LobbyController : MonoBehaviour, ISubscribable
{
    [SerializeField] GameObject _userName;
    [SerializeField] GameObject _userBalance;

    private int _lastGameIdPicked;

    private void Awake()
    {
        NotificationManager.instance.Subscribe(NotificationsEnum.USER_LOCAL_DATA_UPDATED ,this);
        NotificationManager.instance.Subscribe(NotificationsEnum.BALANCE_UPDATED, this);
        NotificationManager.instance.Subscribe(NotificationsEnum.JOIN_ROOM, this);
    }

    private void Start()
    {
       //Costil :) for balance update when return from game. Not needed when Login scene been implemented
       if(UserDataManager.instance.UserData() != null)
        {
            NotificationManager.instance.Dispatch(NotificationsEnum.USER_LOCAL_DATA_UPDATED);
        } 
    }

    public void BuyCoins()
    {
        RequestManager.instance.SendRequest("Balance");
    }

    public void NotificationReceive(NotificationsEnum type, object data)
    {
        switch (type)
        {
            case NotificationsEnum.USER_LOCAL_DATA_UPDATED:
                _userName.GetComponent<Text>().text = UserDataManager.instance.UserData().login;
                _userBalance.GetComponent<Text>().text = UserDataManager.instance.UserData().balance.ToString();
                break;
            case NotificationsEnum.JOIN_ROOM:
                string gameSceneName = String.Format("GameScene_{0}", _lastGameIdPicked);
                SceneManager.LoadScene(gameSceneName, LoadSceneMode.Single);
                break;
        }
    }

    public void GamePick(int id)
    {
        _lastGameIdPicked = id;
        RequestManager.instance.SendRequest("JoinRoom");
    }

    void OnDestroy()
    {
        NotificationManager.instance.Unsubscribe(NotificationsEnum.USER_LOCAL_DATA_UPDATED, this);
        NotificationManager.instance.Unsubscribe(NotificationsEnum.BALANCE_UPDATED, this);
        NotificationManager.instance.Unsubscribe(NotificationsEnum.JOIN_ROOM, this);
    }
}
