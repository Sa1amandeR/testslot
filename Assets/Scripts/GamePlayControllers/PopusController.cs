using System.Collections.Generic;
using UnityEngine;

public class PopusController : MonoBehaviour, ISubscribable
{
    [SerializeField] List<PopupDTO> popups;

    private void Awake()
    {
        NotificationManager.instance.Subscribe(NotificationsEnum.POPUP_SHOW, this);
    }

    public void NotificationReceive(NotificationsEnum type, object data)
    {
        switch (type)
        {
            case NotificationsEnum.POPUP_SHOW:
                GameObject popup = popups.Find((dto) => dto.name == (data as string)).popup;
                Instantiate(popup).transform.SetParent(transform, false);
                break;
        }
    }

    private void OnDestroy()
    {
        NotificationManager.instance.Unsubscribe(NotificationsEnum.POPUP_SHOW, this);
    }
}

enum PopupsNamesEnum
{
    ReturnToLobbyPopup
}
