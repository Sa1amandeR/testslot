using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameFieldController : MonoBehaviour, ISubscribable
{
    private GameItemsConfiguration _itemsConfiguration;
    private List<ReelController> _reelControllers;

    private SpinDTO _currentSpinDto;

    //private readonly int maxStripLength = 7;
    private void Awake()
    {
        NotificationManager.instance.Subscribe(NotificationsEnum.UPDATE_CURRENT_SPIN_DATA, this);
        NotificationManager.instance.Subscribe(NotificationsEnum.UPDATE_REEL_STRIP, this);
        NotificationManager.instance.Subscribe(NotificationsEnum.REEL_STOP_AVAILABLE, this);
        NotificationManager.instance.Subscribe(NotificationsEnum.REEL_STARTED, this);
        NotificationManager.instance.Subscribe(NotificationsEnum.REEL_STOPED, this);
        NotificationManager.instance.Subscribe(NotificationsEnum.SPIN_START, this);
        NotificationManager.instance.Subscribe(NotificationsEnum.SHOW_WINLINE_ITEMS, this);
        NotificationManager.instance.Subscribe(NotificationsEnum.REMOVE_WINLINE_ITEMS, this);
        NotificationManager.instance.Subscribe(NotificationsEnum.LEAVE_ROOM, this);
    }

    void Start()
    {
        _itemsConfiguration = GameObject.Find("MainGameContainer").GetComponent<GameItemsConfiguration>();

        //Set items config and init data to reels
        if (_itemsConfiguration != null)
        {
            _reelControllers = new List<ReelController>();
            _itemsConfiguration.ReelsConfig.ForEach((reelConfig) =>
            {
                reelConfig.ReelController.SetInitializationData(reelConfig.ReelId, reelConfig.ItemsInReel, _itemsConfiguration.ItemsConfig, _itemsConfiguration.ItemTemplatePrefab);
                _reelControllers.Add(reelConfig.ReelController);
            });
        } else
        {
            Debug.Log("GameItemsConfiguration not attached to MainGameContainer");
        }
    }

    public void NotificationReceive(NotificationsEnum type, object data)
    {
        switch (type)
        {
            case NotificationsEnum.UPDATE_CURRENT_SPIN_DATA:
                _currentSpinDto = data as SpinDTO;
                _reelControllers.ForEach((controller) =>
                {
                    string[] reelItems = _currentSpinDto.result.reelsData.Find((reelResult) => reelResult.id == controller.ReelId).symbols;
                    controller.TrySpinResult(reelItems);
                });
                break;
            case NotificationsEnum.REEL_STOP_AVAILABLE:
                ReelController reelToStop = data as ReelController;
                reelToStop.ChangeState(ReelStateEnum.REEL_STOP_SPIN, new StopSpinBehavior());
                break;
            case NotificationsEnum.REEL_START_AVAILABLE:
                //ReelController sender = data as ReelController;

                break;
            case NotificationsEnum.REEL_STARTED:
                int startedReelID = (int)data;
                _reelControllers.Find((reel) => reel.ReelId == startedReelID).ChangeState(ReelStateEnum.REEL_SPIN, new SpinMoveBehavior());
                break;
            case NotificationsEnum.REEL_STOPED:
                int stoppedReelID = (int)data;
                _reelControllers.Find((reel) => reel.ReelId == stoppedReelID).ChangeState(ReelStateEnum.REEL_IDLE, new IdleMoveBehavior());
                if (!_reelControllers.Exists((reel) => reel.CurrentReelState != ReelStateEnum.REEL_IDLE))
                {
                    NotificationManager.instance.Dispatch(NotificationsEnum.SHOW_WINLINES, _currentSpinDto.result.combo);
                    NotificationManager.instance.Dispatch(NotificationsEnum.UPDATE_USER_LOCAL_DATA, _currentSpinDto.reward);
                    NotificationManager.instance.Dispatch(NotificationsEnum.SHOW_WINSCORE, _currentSpinDto.reward);

                }
                break;
            case NotificationsEnum.SPIN_START:
                _reelControllers.ForEach((reel) => reel.ChangeState(ReelStateEnum.REEL_START_SPIN, new StartSpinMoveBehavior()));
                break;
            case NotificationsEnum.SHOW_WINLINE_ITEMS:
                int showlineId = Convert.ToInt32(data as string);
                HighlightWniLineItems(showlineId);
                break;
            case NotificationsEnum.REMOVE_WINLINE_ITEMS:
                int removelineId = Convert.ToInt32(data as string);
                RemoveHighlightWniLineItems(removelineId);
                break;
            case NotificationsEnum.LEAVE_ROOM:
                SceneManager.LoadScene("Lobby", LoadSceneMode.Single);
                break;
        }
    }

    private void HighlightWniLineItems(int lineId)
    {
        ComboResultDTO line = _currentSpinDto.result.combo.Find((combo) => combo.lineId == lineId);
        line.itemsCoordinate.ForEach((itemPositionIndexes) =>
        {
            _reelControllers.Find((reel) => reel.ReelId == itemPositionIndexes.reelId).HighLightWinLineItem(itemPositionIndexes.nestId);
        });
    }

    private void RemoveHighlightWniLineItems(int lineId)
    {
        ComboResultDTO line = _currentSpinDto.result.combo.Find((combo) => combo.lineId == lineId);
        line.itemsCoordinate.ForEach((itemPositionIndexes) =>
        {
            _reelControllers.Find((reel) => reel.ReelId == itemPositionIndexes.reelId).RemoveHighLightWinLineItem(itemPositionIndexes.nestId);
        });
    }

    private void OnDestroy()
    {
        NotificationManager.instance.Unsubscribe(NotificationsEnum.UPDATE_CURRENT_SPIN_DATA, this);
        NotificationManager.instance.Unsubscribe(NotificationsEnum.UPDATE_REEL_STRIP, this);
        NotificationManager.instance.Unsubscribe(NotificationsEnum.REEL_STOP_AVAILABLE, this);
        NotificationManager.instance.Unsubscribe(NotificationsEnum.REEL_STARTED, this);
        NotificationManager.instance.Unsubscribe(NotificationsEnum.REEL_STOPED, this);
        NotificationManager.instance.Unsubscribe(NotificationsEnum.SPIN_START, this);
        NotificationManager.instance.Unsubscribe(NotificationsEnum.SHOW_WINLINE_ITEMS, this);
        NotificationManager.instance.Unsubscribe(NotificationsEnum.REMOVE_WINLINE_ITEMS, this);
        NotificationManager.instance.Unsubscribe(NotificationsEnum.LEAVE_ROOM, this);
    }
}

public enum ReelStateEnum
{
    REEL_IDLE,
    REEL_START_SPIN,
    REEL_SPIN,
    REEL_STOP_SPIN
}
