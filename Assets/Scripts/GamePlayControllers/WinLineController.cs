using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinLineController : MonoBehaviour, ISubscribable
{
    [SerializeField]
    private GameObject LinePrefab;

    private List<GameObject> _allWinLines;
    private List<ReelNestsWroldPointsDTO> _reelsNestPoints;
    private float leftBoundWorld;
    private float rightBoundWorld;

    private void Awake()
    {
        _allWinLines = new List<GameObject>();
        _reelsNestPoints = new List<ReelNestsWroldPointsDTO>();
        NotificationManager.instance.Subscribe(NotificationsEnum.SHOW_WINLINES, this);
        NotificationManager.instance.Subscribe(NotificationsEnum.REEL_NEST_POINTS, this);
        NotificationManager.instance.Subscribe(NotificationsEnum.SPIN_START, this);
    }

    public void NotificationReceive(NotificationsEnum type, object data)
    {
        switch (type)
        {
            case NotificationsEnum.SHOW_WINLINES:
                CreateWinLines(data as List<ComboResultDTO>);
                if (_allWinLines.Count > 0){
                    StartShow();
                } else {
                    NotificationManager.instance.Dispatch(NotificationsEnum.UNLOCK_PANEL);
                }
                break;
            case NotificationsEnum.REEL_NEST_POINTS:
                _reelsNestPoints.Add(data as ReelNestsWroldPointsDTO);
                break;
            case NotificationsEnum.SPIN_START:
                DestroyAllLines();
                _allWinLines.Clear();
                break;
        }
    }

    private void StartShow()
    {
        StartCoroutine(AllLineShow());
    }

    private void CreateWinLines(List<ComboResultDTO> comboResult)
    {
        comboResult.ForEach((combo) =>
        {
            GameObject newLine = Instantiate(LinePrefab);
            newLine.name = combo.lineId.ToString();
            LineRenderer lineRenderer = newLine.GetComponent<LineRenderer>();
            List<Vector3> points = new List<Vector3>();
            Vector3 startPoint = CreateLocalPoint(_reelsNestPoints[0].leftBottomCorner);
            Vector3 endPoint = CreateLocalPoint(_reelsNestPoints[_reelsNestPoints.Count -1].rightBottomCorner);
            Vector3 nestPoint;
            combo.itemsCoordinate.ForEach((itemCoords) =>
            {
                nestPoint = CreateLocalPoint(_reelsNestPoints[itemCoords.reelId].nestPoints[itemCoords.nestId]);
                if(itemCoords.reelId == 0)
                {
                    startPoint.Set(startPoint.x, nestPoint.y, startPoint.z);
                    points.Add(startPoint);
                }

                points.Add(nestPoint);

                if (itemCoords.reelId == (_reelsNestPoints.Count - 1))
                {
                    endPoint.Set(endPoint.x, nestPoint.y, endPoint.z);
                    points.Add(endPoint);
                }
            });
            //points.Sort((point1, point2) => point1.x.CompareTo(point2.x));
            lineRenderer.SetPositions(points.ToArray());
            newLine.SetActive(false);
            newLine.transform.SetParent(transform, false);
            _allWinLines.Add(newLine);
        });
    }

    private Vector3 CreateLocalPoint(Vector3 globalPoint)
    {
        return transform.InverseTransformPoint(globalPoint);
    }

    private void DestroyAllLines()
    {
        StopAllCoroutines();

        _allWinLines.ForEach((line) =>
        {
            RemoveLine(line.name);
            Destroy(line);
        });
    }

    private void RemoveLine(string id)
    {
        List<GameObject> winLines = _allWinLines.FindAll((line) => line.name == id);
        winLines.ForEach((line) => line.SetActive(false));
        NotificationManager.instance.Dispatch(NotificationsEnum.REMOVE_WINLINE_ITEMS, id);
    }

    IEnumerator AllLineShow()
    {
        _allWinLines.ForEach((line) =>
        {
            line.SetActive(true);
            NotificationManager.instance.Dispatch(NotificationsEnum.SHOW_WINLINE_ITEMS, line.name);
        });
        //NotificationManager.instance.Dispatch(NotificationsEnum.SHOW_ALL_WINLINE_ITEMS);
        yield return new WaitForSeconds(2f);

        _allWinLines.ForEach((line) =>
        {
            RemoveLine(line.name);
        });

        NotificationManager.instance.Dispatch(NotificationsEnum.UNLOCK_PANEL);

        Coroutine sequenceShowStart = StartCoroutine(ShowLinesSequence());
        yield return sequenceShowStart;
    }

    //Infinite loop
    IEnumerator ShowLinesSequence()
    {
        int localLineIndex = 0;

        while (localLineIndex < _allWinLines.Count){
            if(_allWinLines[localLineIndex].transform.parent != null)
            _allWinLines[localLineIndex].SetActive(true);
            NotificationManager.instance.Dispatch(NotificationsEnum.SHOW_WINLINE_ITEMS, _allWinLines[localLineIndex].name);

            yield return new WaitForSeconds(2f);
            RemoveLine(_allWinLines[localLineIndex].name);

            localLineIndex++;
            if (localLineIndex == _allWinLines.Count)
            {
                localLineIndex = 0;
            }
        }
    }

    private void OnDestroy()
    {
        NotificationManager.instance.Unsubscribe(NotificationsEnum.SHOW_WINLINES, this);
        NotificationManager.instance.Unsubscribe(NotificationsEnum.REEL_NEST_POINTS, this);
        NotificationManager.instance.Unsubscribe(NotificationsEnum.SPIN_START, this);
    }
}
