using System;
using UnityEngine;
using UnityEngine.UI;

public class PanelController : MonoBehaviour, ISubscribable
{
    private Button _spinButton;
    private Text _balance;
    private Text _freeSpins;
    private Text _bet;
    private Text _win;

    private bool _isSpinAvailable;

    private void Awake()
    {
        NotificationManager.instance.Subscribe(NotificationsEnum.UNLOCK_PANEL, this);
        NotificationManager.instance.Subscribe(NotificationsEnum.LOCK_PANEL, this);
        NotificationManager.instance.Subscribe(NotificationsEnum.USER_LOCAL_DATA_UPDATED, this);
        NotificationManager.instance.Subscribe(NotificationsEnum.SHOW_WINLINES, this);
        NotificationManager.instance.Subscribe(NotificationsEnum.SHOW_WINSCORE, this);
        NotificationManager.instance.Subscribe(NotificationsEnum.SPIN_START, this);
    }

    private void Start()
    {
        _isSpinAvailable = true;
        _spinButton = transform.Find(nameof(PanelElementsNames.Button)).GetComponent<Button>();
        _balance = transform.Find(nameof(PanelElementsNames.Balance)).Find("Count").GetComponent<Text>();
        _freeSpins = transform.Find(nameof(PanelElementsNames.FreeSpinCounter)).Find("Count").GetComponent<Text>();
        _bet = transform.Find(nameof(PanelElementsNames.Bet)).Find("Count").GetComponent<Text>();
        _win = transform.Find(nameof(PanelElementsNames.Win)).Find("Count").GetComponent<Text>();

        UpdateBalance();
    }

    public void OnSpinButtonClick()
    {
        if (_isSpinAvailable)
        {
            if (CheckSpinBalance())
            {
                _isSpinAvailable = false;
                NotificationManager.instance.Dispatch(NotificationsEnum.SPIN_START);
                RequestManager.instance.SendRequest("Spin");
                _spinButton.interactable = false;
            } else
            {
                //Open popup for coins buy
                NotificationManager.instance.Dispatch(NotificationsEnum.POPUP_SHOW, nameof(PopupsNamesEnum.ReturnToLobbyPopup));
                Debug.Log("PSss..Need some coins?");
            }

        }
    }

    public void NotificationReceive(NotificationsEnum type, object data)
    {
        switch (type)
        {
            case NotificationsEnum.LOCK_PANEL:
                _isSpinAvailable = false;
                _spinButton.interactable = false;
                break;
            case NotificationsEnum.UNLOCK_PANEL:
                _isSpinAvailable = true;
                _spinButton.interactable = true;
                break;
            case NotificationsEnum.USER_LOCAL_DATA_UPDATED:
                UpdateBalance();
                break;
            case NotificationsEnum.SHOW_WINSCORE:
                ShowWinScore(data as RewardDTO);
                break;
            case NotificationsEnum.SPIN_START:
                _win.text = "";
                break;
        }
    }

    private void UpdateBalance()
    {
        //_isSpinAvailable = true;
        UserDTO userDto = UserDataManager.instance.UserData();
        _balance.text = userDto.balance.ToString();
        _freeSpins.text = userDto.spins.ToString();
        _bet.text = userDto.spins == 0 ? UserDataManager.instance.GetBetCost().ToString() : "FREE";
    }

    private void ShowWinScore(RewardDTO reward)
    {
        if(reward.coins > 0 || reward.spins > 0)
        {
            string win = String.Format("COINS : {0}\r\nSPINS : {1}", reward.coins, reward.spins);
            _win.text = win;
        }
    }

    private bool CheckSpinBalance()
    {
        UserDTO userDto = UserDataManager.instance.UserData();
        return userDto.balance > UserDataManager.instance.GetBetCost() || userDto.spins > 0;
    }

    private void OnDestroy()
    {
        NotificationManager.instance.Unsubscribe(NotificationsEnum.UNLOCK_PANEL, this);
        NotificationManager.instance.Unsubscribe(NotificationsEnum.LOCK_PANEL, this);
        NotificationManager.instance.Unsubscribe(NotificationsEnum.USER_LOCAL_DATA_UPDATED, this);
        NotificationManager.instance.Unsubscribe(NotificationsEnum.SHOW_WINLINES, this);
        NotificationManager.instance.Unsubscribe(NotificationsEnum.SHOW_WINSCORE, this);
        NotificationManager.instance.Unsubscribe(NotificationsEnum.SPIN_START, this);
    }
}

enum PanelElementsNames
{
    Button,
    FreeSpinCounter,
    Balance,
    Bet,
    Win
}
