using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReelController : MonoBehaviour
{
    [SerializeField] private float durationOnStop;

    private List<ItemConfigDTO> _itemsConfigDTO;
    private GameObject _itemGOTemplate;
    private GameObject _itemsContainer;
    private GameObject _fader;
    private GameObject _winLineItemsContainer;
    private int _visibleItemCount;
    //private List<ReelItemDTO> _currentItemStrip;
    private float _topBoundOfViewPort;
    private float _bottomBoundOfViewport;
    private int _defaultItemsInStrip;
    private bool _isSpinResultSetted;
    private IMoveBehavior _moveBehavior;
    private List<HighlightedNestDTO> _highlightedWinLineItems;

    public int ReelId { get; private set; }
    public ReelStateEnum CurrentReelState { get; private set; }
    public List<ReelItemDTO> CurrentItemStrip { get; private set; }
    public ReelNestsWroldPointsDTO ReelNestsWroldPointsDTO { get; private set; }

    private void Awake()
    {
        _itemsContainer = transform.Find("ItemsViewport").gameObject;
        _fader = transform.Find("Fader").gameObject;
        _winLineItemsContainer = transform.Find("WinLineItems").gameObject;
        _isSpinResultSetted = false;
        _highlightedWinLineItems = new List<HighlightedNestDTO>();
    }

    private void Update()
    {
        if(_moveBehavior != null && CurrentReelState != ReelStateEnum.REEL_IDLE)
        {
            _moveBehavior.Evaluate(CurrentItemStrip);
            if (!_isSpinResultSetted)
            {
                UpdateTailAndHeadItems();
            }
        }
    }

    public void SetInitializationData(int reelId, int reelItemsCount, List<ItemConfigDTO> itemsConfigDTO, GameObject itemGOTemplate)
    {
        ReelId = reelId;
        _visibleItemCount = reelItemsCount;
        _itemsConfigDTO = itemsConfigDTO;
        _itemGOTemplate = itemGOTemplate;

        _topBoundOfViewPort = (_itemsContainer.transform as RectTransform).rect.height;
        _bottomBoundOfViewport = 0;
        _defaultItemsInStrip = reelItemsCount + 4;

        CurrentItemStrip = ItemsGenerator.BuilItemsStrip(_itemsConfigDTO, _defaultItemsInStrip, _itemGOTemplate);
        CurrentItemStrip.ForEach((reelItemDto) =>
        {
            reelItemDto.reelId = reelId;
            reelItemDto.itemGO.transform.SetPositionAndRotation(new Vector3(0, reelItemDto.itemGO.transform.localPosition.y - reelItemDto.height, 0), Quaternion.identity);
            reelItemDto.itemGO.transform.SetParent(_itemsContainer.transform, false);
        });

        ConvertNestPointsToWorld();
        NotificationManager.instance.Dispatch(NotificationsEnum.REEL_NEST_POINTS, ReelNestsWroldPointsDTO);
    }

    public void ChangeState(ReelStateEnum state, IMoveBehavior moveBehavior)
    {
        CurrentReelState = state;
        if (state == ReelStateEnum.REEL_START_SPIN)
        {
            _isSpinResultSetted = false;
            RemoveStoppingPointerFlag();
        }
        _moveBehavior = moveBehavior;
    }

    public void TrySpinResult(string[] result)
    {
        StartCoroutine(SetSpinResult(result));
        //NotificationManager.instance.Dispatch(NotificationsEnum.REEL_STOP_AVAILABLE, ReelId);
    }

    public void HighLightWinLineItem(int nestId)
    {
        _fader.SetActive(true);

        if(!_highlightedWinLineItems.Exists((item) => item.nestId == nestId))
        {
            int highlightItemIndex = CurrentItemStrip.FindIndex((stripItem) => stripItem.isPointForStartStopping) + nestId;
            ReelItemDTO highLigtItemDto = CurrentItemStrip[highlightItemIndex];
            GameObject itemClone = Instantiate(highLigtItemDto.itemGO);

            HighlightedNestDTO newWinItemDto = new HighlightedNestDTO
            {
                nestId = nestId,
                itemGO = itemClone
            };

            newWinItemDto.itemGO.transform.SetParent(_winLineItemsContainer.transform, false);
            _highlightedWinLineItems.Add(newWinItemDto);
        }
    }

    public void RemoveHighLightWinLineItem(int nestId)
    {
        _fader.SetActive(false);
        HighlightedNestDTO highLigtItemDto = _highlightedWinLineItems.Find((item) => item.nestId == nestId);
        if (highLigtItemDto != null)
        {
            _highlightedWinLineItems.Remove(highLigtItemDto);
            Destroy(highLigtItemDto.itemGO);
        }
    }

    private void SetItemsToHead(List<ReelItemDTO> items)
    {
        ReelItemDTO currentStripLastItem;
        float nextItemYCoordinate;
        items.ForEach((itemDto) =>
        {
            itemDto.reelId = ReelId;
            currentStripLastItem = CurrentItemStrip[CurrentItemStrip.Count - 1];
            nextItemYCoordinate = currentStripLastItem.itemGO.transform.localPosition.y + currentStripLastItem.height;
            itemDto.itemGO.transform.SetPositionAndRotation(new Vector3(0, nextItemYCoordinate, 0), Quaternion.identity);
            itemDto.itemGO.transform.SetParent(_itemsContainer.transform, false);
            CurrentItemStrip.Add(itemDto);
        });
    }

    private void RemoveStoppingPointerFlag()
    {
        CurrentItemStrip.ForEach((item) =>
        {
            item.isPointForStartStopping = false;
        });
    }
    //Better place for calculation like this in separated util\manager e.g GeomtryCalculator
    private void UpdateTailAndHeadItems()
    {
        int replacedItemsCount = 0;
        float bottomBoundIndent = _bottomBoundOfViewport - CurrentItemStrip[0].height;//Indent for saving one item under lowermost visible item
        CurrentItemStrip.ForEach((item) =>
        {
            float topCornerYCoordinate = item.itemGO.transform.localPosition.y + item.height;
            if(topCornerYCoordinate <= bottomBoundIndent)
            {
                replacedItemsCount++;
                item.DestroyItem();
            }
        });

        if (replacedItemsCount > 0)
        {
            CurrentItemStrip.RemoveRange(0, replacedItemsCount);

            List<ReelItemDTO> newItems = ItemsGenerator.BuilItemsStrip(_itemsConfigDTO, 1, _itemGOTemplate);
            SetItemsToHead(newItems);
        }
    }

    IEnumerator SetSpinResult(string[] result)
    {
        while (CurrentReelState != ReelStateEnum.REEL_SPIN)
        {
            yield return null;
        }

        List<ReelItemDTO> resultReelItems = new List<ReelItemDTO>();
        for (int i = 0; i < result.Length; i++)
        {
            ReelItemDTO newItem = ItemsGenerator.CreateItemByServerId(_itemsConfigDTO, _itemGOTemplate, result[i]);
            newItem.reelId = ReelId;
            resultReelItems.Add(newItem);
        }

        #region Add one random item to head, excluding duplicate 
        List<ItemConfigDTO> itemsExcludingDuplicate = new List<ItemConfigDTO>();
        ReelItemDTO currentHeadItem = resultReelItems[resultReelItems.Count - 1];
        _itemsConfigDTO.ForEach((configItem) =>
        {
            if (configItem.ServerItemId != currentHeadItem.itemId)
            {
                itemsExcludingDuplicate.Add(configItem);
            }
        });
        List<ReelItemDTO> newReelItem = ItemsGenerator.BuilItemsStrip(itemsExcludingDuplicate, 2, _itemGOTemplate);
        resultReelItems.AddRange(newReelItem);
        #endregion

        resultReelItems[0].isPointForStartStopping = true;
        SetItemsToHead(resultReelItems);

        _isSpinResultSetted = true;

        NotificationManager.instance.Dispatch(NotificationsEnum.REEL_STOP_AVAILABLE, this);
    }

    private void ConvertNestPointsToWorld()
    {
        Vector3 viewPortLeftBottomPointWorld = _itemsContainer.transform.TransformPoint(
            new Vector3(_itemsContainer.transform.localPosition.x - (_itemsContainer.transform as RectTransform).rect.width * 0.5f,
                        _itemsContainer.transform.localPosition.y,
                        0));
        Vector3 viewPortRightBottomPointWorld = _itemsContainer.transform.TransformPoint(
            new Vector3(_itemsContainer.transform.localPosition.x + (_itemsContainer.transform as RectTransform).rect.width * 0.5f,
                        _itemsContainer.transform.localPosition.y,
                        0));

        List<Vector3> nestWorldPoints = new List<Vector3>();
        CurrentItemStrip.ForEach((reelItemDto) =>
        {
            if (reelItemDto.itemGO.transform.localPosition.y >= _bottomBoundOfViewport && reelItemDto.itemGO.transform.localPosition.y < _topBoundOfViewPort)
            {
                RectTransform rectTransform = reelItemDto.itemGO.transform as RectTransform;
                Vector3 worldPos = rectTransform.TransformPoint(
                    rectTransform.localPosition.x,
                    rectTransform.rect.height * 0.5f,
                    0);

                nestWorldPoints.Add(worldPos);
            }
        });

        ReelNestsWroldPointsDTO = new ReelNestsWroldPointsDTO
        {
            reelId = ReelId,
            leftBottomCorner = viewPortLeftBottomPointWorld,
            rightBottomCorner = viewPortRightBottomPointWorld,
            nestPoints = nestWorldPoints
        };
    }

    private void OnDestroy()
    {
        
    }
}
